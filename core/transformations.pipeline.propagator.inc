<?php
/**
 * @file
 * Transformations UI -
 * An interface for managing transformation pipelines.
 *
 * Copyright 2009 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */


/**
 * An "instance" of the pipeline with all of its objects being instantiated.
 *
 * This class can (and should) be used modify pipelines iteratively -
 * it propagates input type and value changes to other operations in the
 * pipeline that are connected to the changed one, and therefore is handy for
 * letting end users edit the pipeline. It's also handy to retrieve information
 * from operations which is not stored in the pipeline configuration itself.
 *
 * You don't need to use it when you know all input/output slots upfront,
 * in that case it's perfectly fine to modify TfPipeline directly.
 */
class TfPipelinePropagator implements TfOperationSlotListener {
  private $pipeline;

  /** An array of actual operation instances, one per operationId
   * (which is the array key for each element).
   * NULL if the operations have not yet been instantiated. */
  private $operations;

  /** Internal state, specifies if changes are currently written back to
   * the pipeline. Used during initialization. */
  private $isPropagating;


  /**
   * Implementation of TfOperation::initialize().
   */
  public function __construct(TfPipeline $pipeline) {
    $this->pipeline = $pipeline;
    $this->operations = array();
    $this->isPropagating = FALSE;

    // Create all operations, already in the correct order of execution.
    foreach ($this->pipeline->operationIds() as $operationId) {
      $this->operations[$operationId] = $this->pipeline->createOperation($operationId);

      // Register as output schema listener in order to make schema changes
      // propagating correctly when input data is changed.
      $this->operations[$operationId]->registerSlotListener($this);
    }

    foreach ($this->operations as $operationId => $operation) {
      foreach ($this->pipeline->targets($operationId) as $outputKey => $targets) {
        foreach ($targets as $targetOperationId => $targetDataSpecs) {
          foreach ($targetDataSpecs as $targetKey => $true) {
            if ($targetOperationId != TfPipeline::Output) { // it's an operation
              $type = $operation->outputProperty($outputKey, 'expectedType');
              $this->operations[$targetOperationId]->setInputType($targetKey, $type);
            }
          }
        }
      }
    }
    $this->isPropagating = TRUE;
  }


  /**
   * Return the pipeline that this pipeline operation is based on.
   *
   * Do check the isPipelineValid() method's result before calling this
   * function, as it will throw an exception when the pipeline is not valid.
   * Remember to only access but not change the pipeline as long as this
   * pipeline operation is in active usage.
   */
  public function pipeline() {
    return $this->pipeline;
  }

  /**
   * Retrieve an operation from this pipeline as TfOperation object.
   *
   * Operation inputs and data types are pre-assigned as good as the
   * implementation allows. (Obviously, data that is only created during the
   * execution of the pipeline cannot be assigned as input at this point.)
   *
   * An exception is thrown when the given operation id does not correspond to
   * an operation in the pipeline.
   */
  public function operation($operationId) {
    return $this->operations[$operationId];
  }

  /**
   * Provide the same functionality as TfPipeline::connect(), only with input
   * types and values propagating to the pipeline when necessary.
   */
  public function connect($sourceEntity, $sourceData, $targetEntity, $targetData) {
    if ($sourceEntity == TfPipeline::Parameter) {
      // Lose any input data and type if any has been set.
      $this->operations[$targetEntity]->setInput($targetData, NULL);
      $this->operations[$targetEntity]->setInputType($targetData, NULL);
      $this->pipeline->connect($sourceEntity, $sourceData, $targetEntity, $targetData);
    }
    elseif ($targetEntity == TfPipeline::Output) {
      $this->pipeline->connect($sourceEntity, $sourceData, $targetEntity, $targetData);
    }
    elseif ($sourceEntity == TfPipeline::Data) {
      $this->operations[$targetEntity]->setInput($targetData, $sourceData);
    }
    else {
      $this->pipeline->connect($sourceEntity, $sourceData, $targetEntity, $targetData);
      $type = $this->operations[$sourceEntity]->outputProperty($sourceData, 'expectedType');
      $this->operations[$targetEntity]->setInputType($targetData, $type);
    }
  }

  /**
   * Provide the same functionality as TfPipeline::disconnectTarget(), only
   * with input types and values propagating to the pipeline when necessary.
   */
  public function disconnectTarget($targetEntity, $targetData) {
    $this->pipeline->disconnectTarget($targetEntity, $targetData);

    if ($targetEntity != TfPipeline::Output) { // is an operation
      $this->operations[$targetEntity]->setInput($targetData, NULL);
      $this->operations[$targetEntity]->setInputType($targetData, NULL);
    }
  }

  /**
   * Provide the same functionality as TfPipeline::disconnectSource(), only
   * with input types and values propagating to the pipeline when necessary.
   */
  public function disconnectSource($sourceEntity, $sourceData) {
    $targets = $this->pipeline->targets($sourceEntity, $sourceData);

    // disconnectTarget() does it all for us, let's just reuse it.
    foreach ($targets as $targetEntity => $targetDataSpecs) {
      foreach ($targetDataSpecs as $targetData => $true) {
        $this->disconnectTarget($targetEntity, $targetData);
      }
    }
  }

  /**
   * Implementation of TfOperationSlotListener::observedInputSchemaChanged().
   */
  public function observedInputChanged(TfOperation $changedOperation, $inputKey, $previousValue) {
    if (!$this->isPropagating) {
      return;
    }
    foreach ($this->operations as $operationId => $operation) {
      if ($changedOperation === $operation) {
        if ($operation->isInputSet($inputKey)) {
          $this->pipeline->connect(
            TfPipeline::Data, $operation->input($inputKey),
            $operationId, $inputKey
          );
        }
        else {
          $this->pipeline->disconnectTarget($operationId, $inputKey);
        }
      }
    }
  }

  /**
   * Implementation of TfOperationSlotListener::observedInputSchemaChanged().
   */
  public function observedInputSchemaChanged(TfOperation $changedOperation, stdClass $keyChanges) {
    if (!$this->isPropagating) {
      return;
    }
    foreach ($this->operations as $operationId => $operation) {
      if ($changedOperation === $operation) {
        $changedKeys = array_merge($keyChanges->updated, $keyChanges->removed);

        foreach ($changedKeys as $changedInputKey) {
          $this->pipeline->disconnectTarget($operationId, $changedInputKey);
        }
      }
    }
  }

  /**
   * Implementation of TfOperationSlotListener::observedOutputSchemaChanged().
   */
  public function observedOutputSchemaChanged(TfOperation $changedOperation, stdClass $keyChanges) {
    foreach ($this->operations as $operationId => $operation) {
      if ($changedOperation === $operation) {
        $changedKeys = array_merge($keyChanges->updated, $keyChanges->removed);
        $existingKeys = array_merge($keyChanges->updated, $keyChanges->added);

        foreach ($this->pipeline->targets($operationId) as $outputKey => $targets) {
          foreach ($targets as $targetOperationId => $targetDataSpecs) {
            foreach ($targetDataSpecs as $targetKey => $true) {
              if ($targetOperationId != TfPipeline::Output) { // it's an operation
                if (in_array($outputKey, $existingKeys)) {
                  $type = $operation->outputProperty($outputKey, 'expectedType');
                  $this->operations[$targetOperationId]->setInputType($targetKey, $type);
                }
                elseif (in_array($outputKey, $keyChanges->removed)) {
                  $this->operations[$targetOperationId]->setInputType($targetKey, NULL);
                }
              }
              if (in_array($outputKey, $changedKeys)) {
                if ($this->isPropagating) {
                  $this->pipeline->disconnectSource($operationId, $outputKey);
                }
              }
            }
          }
        }
      }
    }
  }
}
