<?php

/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_operation_TfStringReplace() {
  return array(
    'category' => t('String manipulation'),
    'label' => t('Replace placeholders in text'),
    'description' => t('Replaces predefined list of placeholders in a larger text by other pieces of text.'),
  );
}


/**
 * Implementation of [module]_operation_[class]().
 */
class TfStringReplace extends TfOperation {
  private $placeholders;

  /**
   * Implementation of TfOperation::inputs().
   */
  public function initialize() {
    $this->placeholders = array();
    parent::initialize();
  }

  public function initializeReplacements() {
    $this->placeholders = array();

    if (!$this->isInputSet('placeholderList')) {
      return;
    }
    foreach ($this->input('placeholderList') as $placeholder) {
      $this->placeholders[] = $placeholder;
    }
  }

  /**
   * Overriding TfOperation::inputChanged().
   */
  protected function inputChanged($inputKey, $previousValue) {
    parent::inputChanged($inputKey, $previousValue);

    if ($inputKey == 'placeholderList') {
      $this->initializeReplacements();
      $this->updateInputSchema();
    }
  }

  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    $inputs = array('template', 'placeholderList');

    foreach ($this->placeholders as $placeholder) {
      $inputs[] = 't:' . $placeholder;
    }
    return $inputs;
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'template') {
      switch ($propertyKey) {
        case 'label':
          return t('Template text');

        case 'label':
          return t('The text that will be transformed by a replacement of placeholders.');

        case 'expectedType':
          return 'php:type:string';
      }
    }
    elseif ($inputKey == 'placeholderList') {
      switch ($propertyKey) {
        case 'label':
          return t('List of placeholders');

        case 'description':
          return t('A plain list of placeholders that (probably) occur in the template text, e.g. array("!title", "%d", "[mytoken]") or whatever. For each of these tokens, a separate input slot will be available.');

        case 'expectedType':
          return 'transformations:list<php:type:string>';
      }
    }
    else {
      $placeholder = substr($inputKey, 2);

      switch ($propertyKey) {
        case 'label':
          return $placeholder;

        case 'description':
          return t('The text that will replace all occurrences of "!placeholder".', array(
            '!placeholder' => $placeholder,
          ));

        case 'expectedType':
          return 'php:type:string';
      }
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('resultText');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'resultText') {
      switch ($propertyKey) {
        case 'label':
          return t('Result text');

        case 'expectedType':
          return 'php:type:string';
      }
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    $template = $this->input('template')->data();
    $replacements = array();

    foreach ($this->placeholders as $placeholder) {
      $replacements[$placeholder] = $this->input('t:' . $placeholder)->data();
    }
    $output->set('resultText', strtr($template, $replacements));
  }
}
